/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2011 Squashtest TM, Squashtest.org
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.registry.client;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.osgi.context.BundleContextAware;

/**
 * @author Gregory Fouquet
 * 
 */
public class PluginBundlesClient implements BundleContextAware, InitializingBean {
	BundleContext bundleContext;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		new Thread(new Runnable() {

			public void run() {
				int readCount = 0;

				while (true) {
					try {
						Thread.sleep(5000);

						System.out.println("~~~ LECTURE DES BUNDLES" + ++readCount);

						for (Bundle bundle : getBundles()) {
							System.out.println(bundle.getSymbolicName());
						}

						System.out.println("~~~ LECTURE DES BUNDLES TERMINEE");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	public List<Bundle> getBundles() {
		Bundle[] bundles = bundleContext.getBundles();

		ArrayList<Bundle> plugins = new ArrayList<Bundle>(bundles.length);

		for (Bundle bundle : bundles) {
			if ("plugin".equals(bundle.getHeaders().get("Squash-TM"))) {
				plugins.add(bundle);
			}
		}

		return plugins;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.osgi.context.BundleContextAware#setBundleContext(org.osgi.framework.BundleContext)
	 */
	public void setBundleContext(BundleContext bundleContext) {
		this.bundleContext = bundleContext;

	}

}
