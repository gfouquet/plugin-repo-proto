/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2011 Squashtest TM, Squashtest.org
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.registry.client;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.util.tracker.BundleTracker;
import org.osgi.util.tracker.BundleTrackerCustomizer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.osgi.context.BundleContextAware;

/**
 * @author Gregory Fouquet
 * 
 */
public class StartedPluginsRegistry implements InitializingBean, BundleContextAware {
	private class PluginTracker implements BundleTrackerCustomizer {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.util.tracker.BundleTrackerCustomizer#addingBundle(org.osgi.framework.Bundle,
		 * org.osgi.framework.BundleEvent)
		 */
		public Object addingBundle(Bundle bundle, BundleEvent event) {
			if ("plugin".equals(bundle.getHeaders().get("Squash-TM"))) {
				return bundle;
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.util.tracker.BundleTrackerCustomizer#modifiedBundle(org.osgi.framework.Bundle,
		 * org.osgi.framework.BundleEvent, java.lang.Object)
		 */
		public void modifiedBundle(Bundle bundle, BundleEvent event, Object object) {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.osgi.util.tracker.BundleTrackerCustomizer#removedBundle(org.osgi.framework.Bundle,
		 * org.osgi.framework.BundleEvent, java.lang.Object)
		 */
		public void removedBundle(Bundle bundle, BundleEvent event, Object object) {
		}
	}

	private BundleContext bundleContext;

	private BundleTracker bundleTracker;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		bundleTracker = new BundleTracker(bundleContext, Bundle.ACTIVE, new PluginTracker());
		bundleTracker.open();

		int readCount = 0;

		while (true) {
			try {
				Thread.sleep(5000);

				System.out.println("~~~ LECTURE DES BUNDLES" + ++readCount);

				for (Bundle bundle : getBundles()) {
					System.out.println(bundle.getSymbolicName());
				}

				System.out.println("~~~ LECTURE DES BUNDLES TERMINEE");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @return
	 */
	private Bundle[] getBundles() {
		final Bundle[] EMPTY = {}; 
		return bundleTracker.getBundles() != null ?  bundleTracker.getBundles() : EMPTY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.osgi.context.BundleContextAware#setBundleContext(org.osgi.framework.BundleContext)
	 */
	public void setBundleContext(BundleContext bundleContext) {
		this.bundleContext = bundleContext;
	}

}
