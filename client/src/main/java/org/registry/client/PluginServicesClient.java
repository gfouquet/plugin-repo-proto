/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2011 Squashtest TM, Squashtest.org
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.registry.client;

import java.util.List;
import java.util.Locale;

import org.registry.spi.Plugin;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * @author Gregory
 * 
 */
public class PluginServicesClient implements InitializingBean {
	private List<Plugin> services;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	public void afterPropertiesSet() throws Exception {
		new Thread(new Runnable() {

			public void run() {
				int readCount = 0;

				int i = 0;
				while (true) {
					LocaleContextHolder.setLocale(Locale.getAvailableLocales()[i++], true);

					try {
						Thread.sleep(5000);

						System.out.println("~~~ LECTURE DES SERVICES " + ++readCount);

						for (Plugin service : getServices()) {
							System.out.println(service.getName());
							System.out.println(service.getLocale());
						}

						System.out.println("~~~ LECTURE DES SERVICES TERMINEE");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	/**
	 * @param services the services to set
	 */
	public void setServices(List<Plugin> services) {
		this.services = services;
	}

	/**
	 * @return the services
	 */
	public List<Plugin> getServices() {
		return services;
	}

}
